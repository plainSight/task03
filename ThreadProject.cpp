// ThreadProject.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <fstream>

using namespace std;

vector<unsigned long> A;
vector<unsigned long> B;
vector<int> result;
mutex readLock;
mutex pushLock;

//Функция для определения, если числа не имеют общих делителей, алгоритмом Евклида
bool GCD(unsigned long a, unsigned long b)
{
	if (b == 0)
		return a == 1;
	return GCD(b, a % b);
}

//Функция, используемая потоками
void findGCD(int iThread, int iTN, int size)
{
	for (int i = iThread; i < size; i += iTN)
	{
		//Мьютекс закрывается при взаимодействии с внешними векторами, чтобы не допустить deadlock-ситуации
		readLock.lock();
		const unsigned long a = A[i];
		const unsigned long b = B[i];
		readLock.unlock();
		if (GCD(a, b))
		{
			pushLock.lock();
			result.push_back(i);
			pushLock.unlock();
		}
	}
}

int main(int argc, char* argv[])
{
	ifstream in;
	ofstream out;
	in.open(argv[2]);
	out.open(argv[3]);
	int size;
	in >> size;
	if (size < 1000)
	{
		cout << "Wrong size: must be >= 1000.";
		return 1;
	}
	
	for (int i = 0; i < size; i++)
	{
		unsigned long val;
		in >> val;
		A.push_back(val);
	}
	for (int i = 0; i < size; i++)
	{
		unsigned long val;
		in >> val;
		B.push_back(val);
	}
	int threadsNumber = atoi(argv[1]);
	thread* threads = new thread[threadsNumber];
	for (int i = 0; i < threadsNumber; i++)
	{
		threads[i] = thread(findGCD, i, threadsNumber, size);
	}
	for (int i = 0; i < threadsNumber; i++)
		threads[i].join();
	for (int element : result)
	{
		out << element << '\n';
	}
}
